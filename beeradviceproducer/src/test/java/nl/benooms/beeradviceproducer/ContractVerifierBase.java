package nl.benooms.beeradviceproducer;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;

public class ContractVerifierBase {

    @Before
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(new BeerAdviceController());
    }
}
