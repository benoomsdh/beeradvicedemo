package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	description("""
Represents a unsuccessful scenario of getting a beer advice

```
given:
	client is old enough
when:
	he applies for a cola
then:
	we'll deny him advice
```

""")
	request {
		method 'POST'
		url '/check'
		body(
				age: 30,
				beverage: "cola"
		)
		headers {
			contentType(applicationJson())
		}
	}
	response {
		status 200
		body("""
			{
				"advice": "NOADVICE"
			}
			""")
		headers {
			contentType(applicationJson())
		}
	}
}
