package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	description("""
Represents a successful scenario of getting a beer

```
given:
	client is not old enough
when:
	he applies for a beer
then:
	we'll deny him the beer
```

""")
	request {
		method 'POST'
		url '/check'
		body(
				age: 5,
				beverage: "beer"
		)
		headers {
			contentType(applicationJson())
		}
	}
	response {
		status 200
		body("""
			{
				"advice": "DENYBEER"
			}
			""")
		headers {
			contentType(applicationJson())
		}
	}
}
