package nl.benooms.beeradviceproducer;

import lombok.Data;

@Data
public class AdviceRequest {
    String beverage;
    int age;
}
