package nl.benooms.beeradviceproducer;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/check")
public class BeerAdviceController {

    private static final String BEER = "beer";
    private static final String POSITIVE_ADVICE = "GRANTBEER";
    private static final String NEGATIVE_ADVICE = "DENYBEER";
    public static final String NO_ADVICE = "NOADVICE";
    private static final int MIN_AGE = 18;

    @PostMapping
    public AdviceResponse getAdvice(@RequestBody AdviceRequest adviceRequest) {
        if (BEER.equalsIgnoreCase(adviceRequest.getBeverage())){
            if (adviceRequest.getAge() >= MIN_AGE) {
                return new AdviceResponse(POSITIVE_ADVICE);
            } else {
                return new AdviceResponse(NEGATIVE_ADVICE);
            }
        } else {
            return new AdviceResponse(NO_ADVICE);
        }
    }
}
