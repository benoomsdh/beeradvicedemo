package nl.benooms.beeradviceproducer;

import lombok.Data;

@Data
public class AdviceResponse {
    String advice;

    public AdviceResponse(String advice) {
        this.advice = advice;
    }
}
