package nl.benooms.beeradviceconsumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureStubRunner(ids = {"nl.benooms:beeradviceproducer:+:stubs:8090"}, workOffline = true)
public class BartenderControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testBartenderRespondingToValidDrinkrequest() {
        // Given
        DrinkRequest drinkRequest = new DrinkRequest("Whatever", 000);
        BartenderResult expectedResult = new BartenderResult("serving Whatever");

        // When
        ResponseEntity<BartenderResult> actualResult = testRestTemplate.postForEntity("/api/getdrink", drinkRequest, BartenderResult.class);

        // Then
        assertThat(actualResult.getStatusCode().is2xxSuccessful());
        assertThat(actualResult.getBody()).isEqualTo(expectedResult);
    }

    @Test
    public void testBartenderRespondingToEmptyDrinkRequest() {
        // Given
        DrinkRequest drinkRequest = new DrinkRequest(null, 999);
        BartenderResult expectedResult = new BartenderResult("no drink requested");

        // When
        ResponseEntity<BartenderResult> actualResult = testRestTemplate.postForEntity("/api/getdrink", drinkRequest, BartenderResult.class);

        // Then
        assertThat(actualResult.getStatusCode().is2xxSuccessful());
        assertThat(actualResult.getBody()).isEqualTo(expectedResult);
    }

    @Test
    public void testBartenderRespondingToBeerDrinkRequestOfChild() {
        // Given
        DrinkRequest drinkRequest = new DrinkRequest("beer", 5);
        BartenderResult expectedResult = new BartenderResult("Not serving beer to you, get lost");

        // When
        ResponseEntity<BartenderResult> actualResult = testRestTemplate.postForEntity("/api/getdrink", drinkRequest, BartenderResult.class);

        // Then
        assertThat(actualResult.getStatusCode().is2xxSuccessful());
        assertThat(actualResult.getBody()).isEqualTo(expectedResult);
    }

    @Test
    public void testBartenderRespondingToBeerDrinkRequestOfAdult() {
        // Given
        DrinkRequest drinkRequest = new DrinkRequest("beer", 18);
        BartenderResult expectedResult = new BartenderResult("serving beer");

        // When
        ResponseEntity<BartenderResult> actualResult = testRestTemplate.postForEntity("/api/getdrink", drinkRequest, BartenderResult.class);

        // Then
        assertThat(actualResult.getStatusCode().is2xxSuccessful());
        assertThat(actualResult.getBody()).isEqualTo(expectedResult);
    }

}