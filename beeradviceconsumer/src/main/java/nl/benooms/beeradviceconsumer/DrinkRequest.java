package nl.benooms.beeradviceconsumer;

import lombok.Data;

@Data
public class DrinkRequest {

    String beverage;
    int age;

    public DrinkRequest(){} // For Jackson

    public DrinkRequest(String beverage, int age) {
        this.beverage = beverage;
        this.age = age;
    }
}
