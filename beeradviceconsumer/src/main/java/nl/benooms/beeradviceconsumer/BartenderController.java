package nl.benooms.beeradviceconsumer;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "api/getdrink")
public class BartenderController {

    private static final String BEER = "beer";
    public static final String SERVING_BEER = "serving beer";
    public static final String NOT_SERVING_BEER = "Not serving beer to you, get lost";

    @Value("${beeradvice.url}")
    private String beeradviceServiceURL;

    @PostMapping
    public BartenderResult getDrink(@RequestBody DrinkRequest drinkRequest) {
        String drinkRequested = drinkRequest.getBeverage();
        if (StringUtils.hasText(drinkRequested)) {
            if(drinkRequested.equalsIgnoreCase(BEER)) {
                return getBeerAdvise(drinkRequest);
            } else {
                return new BartenderResult("serving "+ drinkRequest.getBeverage());
            }

        } else {
            return new BartenderResult("no drink requested");
        }
    }

    private BartenderResult getBeerAdvise(DrinkRequest drinkRequest) {
        RestTemplate template = new RestTemplate();
        AdviceRequest adviceRequest = new AdviceRequest(drinkRequest.getAge(), drinkRequest.getBeverage());
        ResponseEntity<AdviceResponse> adviceResponseResponseEntity = template.postForEntity(beeradviceServiceURL, adviceRequest, AdviceResponse.class);
        if (adviceResponseResponseEntity.getBody().getAdvice().equalsIgnoreCase("GRANTBEER")) {
            return new BartenderResult(SERVING_BEER);
        } else {
            return new BartenderResult(NOT_SERVING_BEER);
        }
    }
}
