package nl.benooms.beeradviceconsumer;

import lombok.Data;

@Data
public class BartenderResult {

    String message;

    public BartenderResult(){} // For Jackson

    public BartenderResult(String message) {
        this.message = message;
    }
}
