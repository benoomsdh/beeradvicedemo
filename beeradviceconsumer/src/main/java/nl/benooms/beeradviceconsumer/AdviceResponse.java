package nl.benooms.beeradviceconsumer;

import lombok.Data;

@Data
public class AdviceResponse {
    String advice;

    public AdviceResponse(){}

    public AdviceResponse(String advice) {
        this.advice = advice;
    }
}
