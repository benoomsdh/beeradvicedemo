package nl.benooms.beeradviceconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeeradviceconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeeradviceconsumerApplication.class, args);
	}
}
