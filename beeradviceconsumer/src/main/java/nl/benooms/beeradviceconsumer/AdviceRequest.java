package nl.benooms.beeradviceconsumer;

import lombok.Data;

@Data
public class AdviceRequest {

    int age;
    String beverage;

    public AdviceRequest(){}

    public AdviceRequest(int age, String beverage) {
        this.age = age;
        this.beverage = beverage;
    }
}
